public with sharing class TestClass {
    
    @future
    public static void testFuture(){
        List<Opportunity> opptysList = [SELECT Id, Description FROM Opportunity];
        for(Opportunity oppty : opptysList){
            oppty.Description = '2Test';            
        }
        update opptysList;
    }

    @future
    @AuraEnabled
    public static void anotherFuture(){
        List<Opportunity> opptysList = [SELECT Id, Description FROM Opportunity];
        for(Opportunity oppty : opptysList){
            oppty.Description = 'another test';           
        }
        update opptysList;
    }

    public static void loopCheck() {
        for (List<Contact> contacts: [SELECT LastName FROM Contact]) {
            System.debug('list loop of size: ' + contacts.size());
            Integer count = 0;
            for(Contact c : contacts) {
                count++;
            }
            System.debug('inside contacts for loop count: ' + count);
        }
        // Integer count = 0;
        // for (Contact contact: [SELECT LastName FROM Contact]) {
        //     count++;            
        // }
        // System.debug('inside contacts for loop count: ' + count);
    }

    public static void createData(){
        List<Contact> contactsList = new List<Contact>();
        List<Contact> contactsList2 = new List<Contact>();
        for(Integer index = 0; index < 200; index++){
            contactsList.add(new Contact(LastName = 'TestContact' + index));
        }
        for(Integer index = 200; index < 400; index++){
            contactsList2.add(new Contact(LastName = 'TestContact' + index));
        }
        insert contactsList;
        insert contactsList2;        
    }

    @AuraEnabled
    public static String getContacts(Integer startFrom, Integer recordsDisplay){
        Integer offsetValue = (startFrom - 1) * recordsDisplay; 
        List<Contact> contactsList = [SELECT Id, Name, Account.Name, Title, Phone, Email FROM Contact ORDER BY Name LIMIT :recordsDisplay OFFSET :offsetValue ];
        return JSON.serialize(contactsList);
    }
}
//TestClass.loopCheck();
// List<Opportunity> res = [SELECT Id, Name FROM Opportunity ORDER BY Name LIMIT 10 OFFSET 10 ];
// for(Opportunity ar : res){
//     System.debug('ar: ' + ar);
// }