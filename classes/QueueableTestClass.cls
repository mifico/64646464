public with sharing class QueueableTestClass implements Queueable{

    public void execute(QueueableContext ctx) {
        List<Account> listOfAccounts = [SELECT Id, Name FROM Account];
        Integer count = 0;       
        for(Account obj : listOfAccounts){
            count++;
        }

        for(Integer index = 0; index < 1000000 ; index++){
            count += index;
        }
        System.debug('objects: ' + count);
    }
}