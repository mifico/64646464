trigger OpportunityTrigger on Opportunity (before update, after update) {
    if(Trigger.isBefore && Trigger.isUpdate){
        for(Opportunity opp : Trigger.new){
            opp.Description  += ' Before-Update ';
        }
    }

    // if(Trigger.isAfter && Trigger.isUpdate){
    //     for(Opportunity opp : Trigger.new){
    //         opp.Description  += ' After-Update ';
    //     }
    // }
}

// Datetime dt = Datetime.now().addHours(3);
// System.debug(dt > Datetime.now());